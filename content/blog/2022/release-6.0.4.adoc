+++
title = "KiCad 6.0.4 Release"
date = "2022-03-18"
draft = false
"blog/categories" = [
    "Release Notes"
]
+++

The KiCad project is proud to announce the latest series 6 stable
release.  The 6.0.4 stable version contains critical bug fixes and
other minor improvements since the previous release.

If you are wondering why there was no "official" 6.0.3 release,
there was a serious issue after 6.0.3 was tagged and some users
had downloaded the unreleased 6.0.3 builds from the KiCad build
server.  The only way to prevent install conflicts was to create
a new 6.0.4 tag with the bug fix and spin new builds.  The KiCad
project would like to apologize for any inconvenience this may
have caused.

<!--more-->

A list of all of the fixed bugs since the 6.0.2 release can be found
on the https://gitlab.com/groups/kicad/-/milestones/13[KiCad 6.0.3
milestone] and
https://gitlab.com/groups/kicad/-/milestones/14[KiCad 6.0.4 milestone]
pages.  This release contains several critical bug fixes so please
consider upgrading as soon as possible.

Version 6.0.4 is made from the
https://gitlab.com/kicad/code/kicad/-/commits/6.0/[6.0] branch with
some cherry picked changes from the development branch.

Packages for Windows, macOS, and Linux are available or will be
in the very near future.  See the
link:/download[KiCad download page] for guidance.

Thank you to all developers, packagers, librarians, document writers,
translators, and everyone else who helped make this release possible.

== Changelog

=== General
- Use temporary directory for intermediate save files that get renamed/copied to destination rather than trying to write to the project directory link:https://gitlab.com/kicad/code/kicad/issues/10747[#10747]
- Nets in the Net Selector dropdown will now "naturally sorted" (A1,A2,A10) rather than "alphabetically" sorted (A1,A10,A2) link:https://gitlab.com/kicad/code/kicad/issues/10534[#10534]
- Avoid clearing the entire OpenGL cache when performing an undo operation
- Fixed height of the message panel in all editors on Windows for multi-monitor setups with varying DPI (link:https://gitlab.com/kicad/code/kicad/-/issues/10681[#10681])
- The most recently used path is remembered for placing images in the Schematic Editor and Drawing Sheet Editor.
- Fixed Grid and Zoom menu bar options not updating translation after an language change with no restart (link:https://gitlab.com/kicad/code/kicad/-/issues/10961[#10961])
- Always import of EAGLE symbol/footprint reference designators in KiCad's terminating number format (0 will be appended if no number is present at the end) (link:https://gitlab.com/kicad/code/kicad/issues/10760[#10760])
- Honor modifier keys (Ctrl, Shift, Alt) with the disambiguation menu (https://gitlab.com/kicad/code/kicad/-/issues/9712[#9712])
- The determination of KICAD6_TEMPLATE_DIR should now be more reliable on all platforms, especially Windows https://gitlab.com/kicad/code/kicad/-/issues/10119[#10119]


=== KiCad Project Manager
- Archives will not be renamed when a project Save-As is performed (https://gitlab.com/kicad/code/kicad/issues/10184[#10184])

=== Gerber Viewer
- Negative objects are now printed correct from the Gerber View
- Edit option under the grid dropdown in the Gerber Viewer will now open the edit dialog (link:https://gitlab.com/kicad/code/kicad/-/issues/10885[#10885])

=== Schematic Editor
- Executing Find & Replace in the Schematic Editor was not undo-able (link:https://gitlab.com/kicad/code/kicad/issues/10824[#10824])
- Fixed symbol library rescue for older v5 schematics in certain conditions that could break schematics (link:https://gitlab.com/kicad/code/kicad/-/issues/10488[#10488])
- Fix the auto-placement of symbol fields not ignoring hidden items (link:https://gitlab.com/kicad/code/kicad/-/issues/10774[#10774]
- The unfold bus menu options are no longer limited to only 128 elements link:https://gitlab.com/kicad/code/kicad/-/issues/10529[#10529]
- Actually implemented the visible but not implemented "Duplicate symbol" hotkey option link:https://gitlab.com/kicad/code/kicad/-/issues/11034[#11034]
- Fix ERC wrongly complaining about symbols that had their pin names and numbers hidden individually in the schematic editor link:https://gitlab.com/kicad/code/kicad/-/issues/11018[#11018]
- The netlist export window will now always remember the last used netlist type after use (link:https://gitlab.com/kicad/code/kicad/issues/11094[#11094], link:https://gitlab.com/kicad/code/kicad/-/issues/10910[#10910])
- Reduce and improve the schematic file ordering of elements to reduce excessive diffs between edits
- Ensure file paths in sheets always uses backwards slashes to avoid cross-platform issues (link:https://gitlab.com/kicad/code/kicad/issues/10964[#10964])
- Fix a crash when text vars of a sheet were referenced on a schematic instance of said sheet (link:https://gitlab.com/kicad/code/kicad/-/issues/11000[#11000])
- Handle switching between h/v mode better while drawing lines (link:https://gitlab.com/kicad/code/kicad/-/issues/10859[#10859])
- Don't crash on highlighting of a net during a drag operation (link:https://gitlab.com/kicad/code/kicad/issues/10866[#10866])
- Fixed crash when adding rows to the pin table and then pasting to a unit
- The logic for trimming wires on place over pins is now dependent on how many pins are intersected (link:https://gitlab.com/kicad/code/kicad/-/issues/10909[#10909])

=== Symbol Editor
- Pins in the symbol editor now show the connection point (link:https://gitlab.com/kicad/code/kicad/issues/10742[#10742])
- Update alternate pin assignments in other units when editing the symbol properties of a multi-unit symbol (link:https://gitlab.com/kicad/code/kicad/issues/10849[#10849])
- Sync all fields between units when symbol properties are edited (link:https://gitlab.com/kicad/code/kicad/issues/10610[#10610])
- Unit 0 of a symbol is now always declared in the file format even if only a single unit is used link:https://gitlab.com/kicad/code/kicad/issues/10888[#10888]
- Fix a crash that can occur editing name and description fields in the symbol editor link:https://gitlab.com/kicad/code/kicad/issues/11060[#11060]
- Fix up handling of symbol loading, it will now show a warning info bar it was cancelled and actually cancel link:https://gitlab.com/kicad/code/kicad/issues/8372[#8372]
- Grid snapping will disable when the ctrl/cmd key is pressed to match all other editors link:https://gitlab.com/kicad/code/kicad/issues/9995[#9995]
- It is now possible to paste into the pin table when a column is hidden link:https://gitlab.com/kicad/code/kicad/-/issues/10168[#10168]

=== PCB Editor
- DRC may have run against out of date bounding boxes for zones (link:https://gitlab.com/kicad/code/kicad/issues/10821[#10821])
- Fix case where self intersecting zones may be created from a merge (link:https://gitlab.com/kicad/code/kicad/issues/10466[#10466])
- Fixed the inability to undo a failed zone merge (link:https://gitlab.com/kicad/code/kicad/issues/10466[#10466])
- Added error messages for zone merging (link:https://gitlab.com/kicad/code/kicad/issues/10466[#10466])
- Updated custom rule syntax help to have a clearer differential pair example
- Don't lock the OpenGL context twice (link:https://gitlab.com/kicad/code/kicad/-/issues/10840[#10840])
- Fixed the inability to create 180 degree arcs (link:https://gitlab.com/kicad/code/kicad/issues/10841[#10841])
- Netlist import must now be explicitly triggered in the dialog
- Gerber job file writer will now include user layers 1 thru 9 (link:https://gitlab.com/kicad/code/kicad/issues/10878[#10878])
- Improved board statistics drill table column sizing
- Add support for Dwg_User, Cmts_User, Eco1_User, Eco2_User in the gerber jobfile export
- Improve arc handling with push and shove routing
- 'Delete redundant graphics' in the pcb editor under Cleanup graphics will now delete arcs (link:https://gitlab.com/kicad/code/kicad/issues/10955[#10955])
- Prevent merging of tracks that go across pads
- Maintain connectivity while cleaning tracks/vias (link:https://gitlab.com/kicad/code/kicad/issues/10780[#10780])
- Fixed missing account of dielectric layers in the calculation of the board thickness in the board stackup (link:https://gitlab.com/kicad/code/kicad/issues/10899[#10899])
- Fix a crash when placing a via in certain circumstances link:https://gitlab.com/kicad/code/kicad/issues/10732[#10732]
- Fixed case where converting copper lines into actual track segments would fail link:https://gitlab.com/kicad/code/kicad/issues/11025[#11025]
- Fixed situation where the reference designator was not visible without selecting a PCB component link:https://gitlab.com/kicad/code/kicad/issues/11026[#11026]
- Fixed case where merge colinear tracks will want to erase tracks split by vias and other connectable elements link:https://gitlab.com/kicad/code/kicad/issues/10916[#10916]
- Allow rotating PCB items that aren't placed yet on empty boards link:https://gitlab.com/kicad/code/kicad/issues/10581[#10581]
- Improve handling pads/vias in zones which take priority over other zones link:https://gitlab.com/kicad/code/kicad/issues/11036[#11036]
- Ensure the arc shape data does not change when resaved back to disk with no changes
- Remove extraneous floating point digits when moving arc objects link:https://gitlab.com/kicad/code/kicad/issues/10739[#10739]
- Fixed leftover zone material not being removed from under vias in certain circumstances
- The default line width for DXF import will no longer wrongly get scaled by the scale factor option by an extra amount (link:https://gitlab.com/kicad/code/kicad/-/issues/11122[#11122])
- Redraw pads when mask or paste layer is visible after board setup (link:https://gitlab.com/kicad/code/kicad/issues/11079[#11079])
- Don't render vias that don't cross the selected layer in high-contrast mode (link:https://gitlab.com/kicad/code/kicad/issues/8740[#8740])
- Ensure the Get and Move Footprint dialog has focus on the input box on open (link:https://gitlab.com/kicad/code/kicad/-/issues/10230[#10230])
- Closing the zone properties dialog will no longer drop all ratsnest lines (link:https://gitlab.com/kicad/code/kicad/-/issues/10608[#10608])
- The spacebar will no longer close the footprint browser window on GTK (link:https://gitlab.com/kicad/code/kicad/-/issues/10633[#10633], https://gitlab.com/kicad/code/kicad/-/issues/5714[#5714])
- The break wire and labeling functions will no longer be enabled on multi-wire selections link:https://gitlab.com/kicad/code/kicad/-/issues/10934[#10943]
- Inner layers will no longer get stuck toggled off/on in the Appearance controls (link:https://gitlab.com/kicad/code/kicad/-/issues/10977[#10977])
- Reset the drill count list before updating the board statistics table for drills (link:https://gitlab.com/kicad/code/kicad/issues/10984[#10984])
- Fix the tab-key order of operations in the Footprint Properties dialog (link:https://gitlab.com/kicad/code/kicad/-/issues/10978[#10978])
- Fix a crash when a footprint is exported to the board editor from the footprint editor
- Avoid spurious disambiguation menus appearing when trying to drag solitary track corners link:https://gitlab.com/kicad/code/kicad/issues/10745[#10745]
- Fix a crash when empty SVGs were fed for import
- Fix plotting arcs in gerbers in certain circumstances
- Schematic symbol value fields should now always resolve variables link:https://gitlab.com/kicad/code/kicad/-/issues/11003[#11003]
- Holes in SVG polygonal shapes can now be imported
- Multi-path shapes can now be imported from SVGs
- Flipping a zone maintains all previously selected layers link:https://gitlab.com/kicad/code/kicad/-/issues/10621[#10621]
- Edit Track & Via Properties dialog will now batch the updates to items as a significant performance fix for large boards link:https://gitlab.com/kicad/code/kicad/-/issues/10905[#10905]
- The status bar will maintain relevant information while routing link:https://gitlab.com/kicad/code/kicad/-/issues/10068[#10068]
- The net inspector column for name should no longer open collapsed
- DRC rule areas can now be used on all physical layers
- The Appearance & Selector filter panels will now update when the language is switched https://gitlab.com/kicad/code/kicad/-/issues/10040[#10040]
- It is no longer possible to edit another footprint pad while already having one open to edit https://gitlab.com/kicad/code/kicad/-/issues/10209[#10209]

=== Plugin & Content Manager
- Fixed "wxT" appearing in the Plugin & Content manager panes in various places

=== 3D Viewer
- Fix issues with step files having graphical artifacts in the form of wrong colors or visible triangles (link:https://gitlab.com/kicad/code/kicad/-/issues/6107[#6107], link:https://gitlab.com/kicad/code/kicad/-/issues/9416[#9416], https://gitlab.com/kicad/code/kicad/-/issues/9835
[#9835], https://gitlab.com/kicad/code/kicad/-/issues/10554[#10554])

=== Python
- Fix case where DRC executed via python differed from the UI triggered DRC (link:https://gitlab.com/kicad/code/kicad/issues/10221[#10221])
- Update PadArray.py SetAttribute invocation for 6.0 types
- Fix python plugins getting loaded twice and sys.path getting duplicated
- Allow multiple projects to be loaded by standalone python scripting concurrently link:https://gitlab.com/kicad/code/kicad/-/issues/10540[#10540]
- Allow saving PCBs in python when the kicad project is currently open (link:https://gitlab.com/kicad/code/kicad/-/issues/10540[#10540])

=== macOS Platform
- Prevent python on macOS from attempting to say .pyc files to its bundle which breaks code signing

=== Windows Platform
- ".kiface" files in the bin folder on Windows now end with ".dll" to reflect their actual file type
- Stray DLLs will no longer be loaded from the current working directory on Windows
- The Windows Error Reporting dialog should now always show on Windows after a crash
