+++
title = "KiCad 6.0.6 Release"
date = "2022-06-19"
draft = false
"blog/categories" = [
    "Release Notes"
]
+++

The KiCad project is proud to announce the latest series 6 stable
release.  The 6.0.6 stable version contains critical bug fixes and
other minor improvements since the previous release.

<!--more-->

A list of all of the fixed issues since the 6.0.5 release can be found
on the https://gitlab.com/groups/kicad/-/milestones/16[KiCad 6.0.6
milestone] page. This release contains several critical bug fixes so please
consider upgrading as soon as possible.

Version 6.0.6 is made from the
https://gitlab.com/kicad/code/kicad/-/commits/6.0/[6.0] branch with
some cherry picked changes from the development branch.

Packages for Windows, macOS, and Linux are available or will be
in the very near future.  See the
link:/download[KiCad download page] for guidance.

Thank you to all developers, packagers, librarians, document writers,
translators, and everyone else who helped make this release possible.

== Changelog

=== General
- Fix intermittent QA crash. https://gitlab.com/kicad/code/kicad/-/issues/9888[#9888]
- Disable background color when printing in black & white. https://gitlab.com/kicad/code/kicad/-/issues/11180[#11180]
- Fix stock templates path for flatpak. https://gitlab.com/kicad/code/kicad/-/issues/11174[#11174]
- Allow closing PCM progress windows after installation from ZIP file. https://gitlab.com/kicad/code/kicad/-/issues/11601[#11601]
- Properly enable and disable the apply and discard buttons in the PCM. https://gitlab.com/kicad/code/kicad/-/issues/10761[#10761]
- Fix net highlighting between schematic and board editors. https://gitlab.com/kicad/code/kicad/-/issues/11493[#11493]

=== Schematic Editor
- Fix crash when exporting Spice Netlist to read-only directory. https://gitlab.com/kicad/code/kicad/-/issues/11516[#11516]
- Fix "select previous symbol" toolbar state in footprint assignment tool. https://gitlab.com/kicad/code/kicad/-/issues/11509[#11509]
- Update selection filter title bar with language changes. https://gitlab.com/kicad/code/kicad/-/issues/11421[#11421]
- Fix annotation sheet page number bug. https://gitlab.com/kicad/code/kicad/-/issues/11295[#11295]
- Fix text and graphics properties dialog layout. https://gitlab.com/kicad/code/kicad/-/issues/11490[#11490]
- Fix find in sub-sheets. https://gitlab.com/kicad/code/kicad/-/issues/11424[#11424]
- Fix automatic field placement bug. https://gitlab.com/kicad/code/kicad/-/issues/11529[#11529]
- Implement cross-references for labels. https://gitlab.com/kicad/code/kicad/-/issues/11564[#11564]
- Fix schematic hierarchy navigator crash. https://gitlab.com/kicad/code/kicad/-/issues/11550[#11505]
- Resolve title variable when plotting. https://gitlab.com/kicad/code/kicad/-/issues/11608[#11608]
- https://gitlab.com/kicad/code/kicad/-/commit/2553da38b4c33b626d10b976ca99676014fb4641[Fix parsing of CADSTAR color attributes.]
- Fix symbol remap issue. https://gitlab.com/kicad/code/kicad/-/issues/11563[#11563]
- Fix Eagle importer page number handling. https://gitlab.com/kicad/code/kicad/-/issues/11409[#11409]
- Plot alternate pin definitions correctly. https://gitlab.com/kicad/code/kicad/-/issues/11728[#11728]
- Fix file name in symbol library table file dialog. https://gitlab.com/kicad/code/kicad/-/issues/11821[#11821]

=== Symbol Editor
- Fix segmentation fault trying to edit text field in new unit. https://gitlab.com/kicad/code/kicad/-/issues/11569[#11569]
- Fix crash when trying to save symbol to read-only location. https://gitlab.com/kicad/code/kicad/-/issues/11566[#11566]
- Fix library symbol properties dialog tab selection bug. https://gitlab.com/kicad/code/kicad/-/issues/11417[#11417]
- Unable to create any graphic elements after selecting "Add pin" function. https://gitlab.com/kicad/code/kicad/-/issues/11607[#11607]
- Fix duplicate pin number test. https://gitlab.com/kicad/code/kicad/-/issues/11660[#11660]

=== PCB Editor
- Fix fix a truncation error when converting mils to IU in P-CAD importer. https://gitlab.com/kicad/code/kicad/-/issues/11380[#11380]
- Fix duplicate messages when updating schematic from PCB. https://gitlab.com/kicad/code/kicad/-/issues/11530[#11530]
- Fix lost of focus after clicking on a choice in toolbar. https://gitlab.com/kicad/code/kicad/-/issues/11103[#11103]
- Remove highlighting track that doesn't have net name. https://gitlab.com/kicad/code/kicad/-/issues/10364[#10364]
- Update layer colors correctly when changing color themes. https://gitlab.com/kicad/code/kicad/-/issues/11261[#11261]
- Update inactive layers label when changing languages. https://gitlab.com/kicad/code/kicad/-/issues/11435[#11435]
- Fix blind via visibility issue. https://gitlab.com/kicad/code/kicad/-/issues/11429[#11429]
- Fix reference and value variable expansion. https://gitlab.com/kicad/code/kicad/-/issues/11476[#11476]
- Change default symbol matching to use UUID instead of reference when back annotating schematic. https://gitlab.com/kicad/code/kicad/-/issues/11382[#11382]
- Use correct back side placement angle using experimental Gerber export option. https://gitlab.com/kicad/code/kicad/-/issues/11621[#11621]
- Fix crash on layer visibility change. https://gitlab.com/kicad/code/kicad/-/issues/11629[#11629]
- Make P-CAD import case insensitive. https://gitlab.com/kicad/code/kicad/-/issues/11652[#11652]
- Import P-CAD footprints to correct layer. https://gitlab.com/kicad/code/kicad/-/issues/11652[#11652]
- Fix invalid zone filling on rotated footprint text. https://gitlab.com/kicad/code/kicad/-/issues/11714[#11714]
- Fix zone merge intersection algorithm. https://gitlab.com/kicad/code/kicad/-/issues/11492[#11492]
- Use board set up solder mask color when exporting to STEP. https://gitlab.com/kicad/code/kicad/-/issues/11565[#11565]
- Fix VRML layer color export. https://gitlab.com/kicad/code/kicad/-/issues/11627[#11627]
- Import EAGLE octagonal pads correctly. https://gitlab.com/kicad/code/kicad/-/issues/11664[#11664]
- Fix skew tuning track length bug. https://gitlab.com/kicad/code/kicad/-/issues/11710[#11710]
- Prevent length and skew tuning dialog values from becoming negative. https://gitlab.com/kicad/code/kicad/-/issues/10870[#10870]
- Fix router DRC violation when pushing tracks against complex board outline. https://gitlab.com/kicad/code/kicad/-/issues/11365[#11365]
- Fix router walk around arc to board edge issue. https://gitlab.com/kicad/code/kicad/-/issues/10536[#10536]
- Fix router not on grid issue. https://gitlab.com/kicad/code/kicad/-/issues/10710[#10710]
- Do not translate layer names is appearance manager. https://gitlab.com/kicad/code/kicad/-/issues/11715[#11715]
- https://gitlab.com/kicad/code/kicad/-/commit/895314b639c1dc103d42ab0dff79fdf97990cc45[Fix hole clearance to improve router walk around mode.]
- https://gitlab.com/kicad/code/kicad/-/commit/7053981835101e8740a0a880bc29c83a4ab651bd[Maintain visibility state when changing layer count.]
- Handle rotated pad thermal spokes correctly. https://gitlab.com/kicad/code/kicad/-/issues/11765[#11765]
- Many router fixes.
https://gitlab.com/kicad/code/kicad/-/commit/4d5b5d3791f4aa6f15889db4645d95c0480953de[1],
https://gitlab.com/kicad/code/kicad/-/commit/4694a2622ebefef9b3501bd1bb5d52542bcd3a5a[2],
https://gitlab.com/kicad/code/kicad/-/commit/92ba60628e0d174f5e97a3bef834ce05149ac44d[3],
https://gitlab.com/kicad/code/kicad/-/commit/2be352b9f9ba67d649bca5ccfeef8c2ecd11f975[4], and
https://gitlab.com/kicad/code/kicad/-/commit/8afb8190af09f4875ea3c3048188962ba538fe80[5].
- Show error message when PCB rule pad type is invalid. https://gitlab.com/kicad/code/kicad/-/issues/11423[#11423]
- Export micro-vias correctly to Hyperlynx. https://gitlab.com/kicad/code/kicad/-/issues/11692[#11692]
- Fix documentation link bug in footprint chooser. https://gitlab.com/kicad/code/kicad/-/issues/11403[#11403]
- Fix footprint text bounding box rotation issue. https://gitlab.com/kicad/code/kicad/-/issues/8728[#8728]
- https://gitlab.com/kicad/code/kicad/-/commit/b59c334edb68d1afae1c15c8ce727dd8073d7323[Improve router "kink" robustness].
- Fix P-CAD layer import issue. https://gitlab.com/kicad/code/kicad/-/issues/11750[#11750]
- Add P-CAD "pourOutline support. https://gitlab.com/kicad/code/kicad/-/issues/11749[#11749]

=== Footprint Editor
- Fix crash when editing custom pad shapes. https://gitlab.com/kicad/code/kicad/-/issues/11731[#11731]

=== 3D Viewer
- https://gitlab.com/kicad/code/kicad/-/commit/592c2c687268c6eef91c1ba1107812010b1725c9[Fix crash when closing 3D viewer.]

=== Linux Platform
- Prevent focus stealing in some Linux window managers. https://gitlab.com/kicad/code/kicad/-/issues/10809[#10809]

=== Windows
- Updated to wx 3.1.6 which has improved behavior for high-DPI screens, reduced (but not fixed) IME freezes and eliminated the random locale swaps.