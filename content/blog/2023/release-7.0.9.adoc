+++
title = "KiCad 7.0.9 Release"
date = "2023-11-08"
draft = false
"blog/categories" = [
    "Release Notes"
]
+++

The KiCad project is proud to announce the version 7.0.9 bug fix release.
The 7.0.9 stable version contains critical bug fixes and other minor
improvements since the previous release.

<!--more-->

A list of all of the fixed issues since the 7.0.8 release can be found
on the https://gitlab.com/groups/kicad/-/milestones/31[KiCad 7.0.9
milestone] page. This release contains several critical bug fixes so please
consider upgrading as soon as possible.

Version 7.0.9 is made from the
https://gitlab.com/kicad/code/kicad/-/commits/7.0/[7.0] branch with
some cherry picked changes from the development branch.

Packages for Windows, macOS, and Linux are available or will be
in the very near future.  See the
link:/download[KiCad download page] for guidance.

Thank you to all developers, packagers, librarians, document writers,
translators, and everyone else who helped make this release possible.

== Changelog

=== General
- https://gitlab.com/kicad/code/kicad/-/commit/686048dcce5bc90beee962c394a857342c0ec674[Fix orphaned lock files].
- Set focus back to the main window after dialog is dismissed. https://gitlab.com/kicad/code/kicad/-/issues/15603[#15603]
- Draw zero sized rectangles on OpenGL canvas. https://gitlab.com/kicad/code/kicad/-/issues/15850[#15850]
- Fix zero sized or too thick rectangles when plotting to PDF. https://gitlab.com/kicad/code/kicad/-/issues/15862[#15862]
- https://gitlab.com/kicad/code/kicad/-/commit/4f5fca4e67c1a90b693dfe895cc5afc755204935[Prevent rectangles from always being filled when plotting to HPGL].
- https://gitlab.com/kicad/code/kicad/-/commit/cbf1b2af4799672fe4a644424dc0a3f0c2931782[Fix random lines connecting to circles when plotting in HPGL].
- https://gitlab.com/kicad/code/kicad/-/commit/75c758e5f4eddfed95e2ae52f6655572be2bb492[Improve bitmap caching performance].
- Don't show integer overflow errors in release builds. https://gitlab.com/kicad/code/kicad/-/issues/15529[#15529]

=== Schematic Editor
- Fix crash when updating symbols. https://gitlab.com/kicad/code/kicad/-/issues/15715[#15715]
- Fix crash when placing symbol. https://gitlab.com/kicad/code/kicad/-/issues/15728[#15728]
- Do not allow file overwrite when importing third party schematics. https://gitlab.com/kicad/code/kicad/-/issues/15570[#15570]
- Implement printing and plotting for directive specific label shapes. https://gitlab.com/kicad/code/kicad/-/issues/15650[#15650]
- Handle flipped horizontal and vertical justifications in fields grid. https://gitlab.com/kicad/code/kicad/-/issues/15677[#15677]
- Support database columns with numeric data. https://gitlab.com/kicad/code/kicad/-/issues/15662[#15662]
- Fix crash when loading schematic with a symbol the has a anme containing {slash} instead of /.  https://gitlab.com/kicad/code/kicad/-/issues/15791[#15791]
- Handle justification when hit testing fields. https://gitlab.com/kicad/code/kicad/-/issues/15722[#15722]
- https://gitlab.com/kicad/code/kicad/-/commit/8f4b80959992b61c05ee77538fa3d8794b052469[Fix the insufficient privileges message in symbol remap dialog].
- https://gitlab.com/kicad/code/kicad/-/commit/0547c7ba91e1cc856e7a80749acb834c8945ea73[Fix loading legacy schematic files with ERC markers].

=== Symbol Editor
- Synchronize value field when renaming a symbol. https://gitlab.com/kicad/code/kicad/-/issues/15621[#15621]
- https://gitlab.com/kicad/code/kicad/-/commit/0eca698a39183c0a20a3c0a71bc623efe85724d2[Update title bar when renaming the canvas symbol].
- Synchronize value field when performing symbol "Save As". https://gitlab.com/kicad/code/kicad/-/issues/15647[#15647]
- Handle read only libraries when saving symbol from canvas. https://gitlab.com/kicad/code/kicad/-/issues/15519[#15519]
- Show empty libraries in library tree panel. https://gitlab.com/kicad/code/kicad/-/issues/15875[#15875]

=== Simulator
- Fix small signal AC simulation failure when using current source. https://gitlab.com/kicad/code/kicad/-/issues/15626[#15626]
- Include spice model comment lines in pin assingment "Reference" window. https://gitlab.com/kicad/code/kicad/-/issues/15660[#15660]
- Don't turn an empty string into a '0' in spice model editor. https://gitlab.com/kicad/code/kicad/-/issues/15871[#15871]

=== Board Editor
- Do not create duplicate layer names when importing Altium PCB. https://gitlab.com/kicad/code/kicad/-/issues/15583[#15583]
- https://gitlab.com/kicad/code/kicad/-/commit/909d19937939c203e1114fe948b808a731b493d8[Save flip board status in custom preset].
- Fix selection of graphics only footprints in single layer view modes. https://gitlab.com/kicad/code/kicad/-/issues/15284[#15284]
- https://gitlab.com/kicad/code/kicad/-/commit/ad9f5f4dd7457cd28b0688201f3100fc94ffc8e6[Extend boundary size of canvas].
- https://gitlab.com/kicad/code/kicad/-/commit/bb579294a0f3b7041fd99708c081c752a45c4797[Fix search crash when changing boards].
- Fix crash when exporting GERBER files. https://gitlab.com/kicad/code/kicad/-/issues/15782[#15782]
- Fix incorrect arc in some cases when s-expression uses mid-points. https://gitlab.com/kicad/code/kicad/-/issues/15694[#15694]
- https://gitlab.com/kicad/code/kicad/-/commit/33da35db040abf7333690af66ee422c3525909f0[Fix bug in custom rules evaluator].
- Don't import Altium objects to disabled layers. https://gitlab.com/kicad/code/kicad/-/issues/15586[#15586]
- Perform full high contrast update of vias and pads if net names are shown. https://gitlab.com/kicad/code/kicad/-/issues/15657[#15657]
- Fix rendering issue with vias not connected on all layers. https://gitlab.com/kicad/code/kicad/-/issues/15329[#15329]
- Correct path of PCB drawing sheet file on "Save As". https://gitlab.com/kicad/code/kicad/-/issues/15551[#15551]
- Repair stand alone application 3D plugin path resolution. https://gitlab.com/kicad/code/kicad/-/issues/15322[#15322]
- Start measuring from first mouse drag location instead of mouse click location. https://gitlab.com/kicad/code/kicad/-/issues/15623[#15623]
- Fix errors in calculating tuning length. https://gitlab.com/kicad/code/kicad/-/issues/10614[#10614]
- Fix broken drawing sheet rendering when board view is flipped. https://gitlab.com/kicad/code/kicad/-/issues/15768[#15768]
- Fix decreasing via size giving option not found in design rules. https://gitlab.com/kicad/code/kicad/-/issues/15774[#15774]
- Improved difficult footprint selection issue. https://gitlab.com/kicad/code/kicad/-/issues/15813[#15813]
- Fix a regression causing missing polygons when importing Eagle boards. https://gitlab.com/kicad/code/kicad/-/issues/15829[#15829]
- https://gitlab.com/kicad/code/kicad/-/commit/87c8688f706372aa1fc97382475f8a7f8b2f2715[Reduce the pen width used to plot oval shapes for a better look when generating drill map files].
- https://gitlab.com/kicad/code/kicad/-/commit/9159f5a00456b528e5cd15139e381c19975b9557[Restore fill polygons on proper layers when flipping zones].
- Fix crash for zero length lines on edge cuts layer when running DRC. https://gitlab.com/kicad/code/kicad/-/issues/15865[#15865]
- Do not create unexpected zones on solder mask layers. https://gitlab.com/kicad/code/kicad/-/issues/15847[#15847]
- Do not change blind/buried via layers when adding new layers. https://gitlab.com/kicad/code/kicad/-/issues/15856[#15856]
- https://gitlab.com/kicad/code/kicad/-/commit/d4ec455ce00341e0e9bfbde54159b389e5c87da4[Fix loading through hole pads when drill token is missing].
- Fix a crash when using length tuning tools. https://gitlab.com/kicad/code/kicad/-/issues/15874[#15874]
- Fix invisible rats nest rendering while moving footprint on white background. https://gitlab.com/kicad/code/kicad/-/issues/15841[#15841]
- Honour custom layer names for top and bottom copper layers in custom rules. https://gitlab.com/kicad/code/kicad/-/issues/15835[#15835]
- Include solder mask in STEP export stackup height. https://gitlab.com/kicad/code/kicad/-/issues/15632[#15632]
- Fix crash when selecting a zone on inner layer and reducing the number of copper layers. https://gitlab.com/kicad/code/kicad/-/issues/15866[#15866]
- Do not route tracks tighter than DRC allows. https://gitlab.com/kicad/code/kicad/-/issues/14898[#14898]
- Fix DRC crash. https://gitlab.com/kicad/code/kicad/-/issues/15853[#15853]
- Fix crash when linking footprints to external library. https://gitlab.com/kicad/code/kicad/-/issues/15797[#15797]
- Fix crash when using "Duplicate Zone onto Layer..." if the original zone has multiple copper layers. https://gitlab.com/kicad/code/kicad/-/issues/15916[#15916]
- Fix false positive when performing footprint parity DRC. https://gitlab.com/kicad/code/kicad/-/issues/15917[#15917]
- https://gitlab.com/kicad/code/kicad/-/commit/d3d23d18fb9276fd16cd4faf671e3df55694bf98[Fix footprints being difficult to select in high-contrast modes].
- Fix broken via shoving when routing. https://gitlab.com/kicad/code/kicad/-/issues/15840[#15840]
- Don't run DRC checks against reference images. https://gitlab.com/kicad/code/kicad/-/issues/15932[#15932]

=== Footprint Editor
- Show empty libraries in library tree panel. https://gitlab.com/kicad/code/kicad/-/issues/15875[#15875]
- Fix crash when copying and pasting all objects. https://gitlab.com/kicad/code/kicad/-/issues/15930[#15930]

=== Gerber Viewer
- Fix case sensitivity for gerber file extension  wildcards. https://gitlab.com/kicad/code/kicad/-/issues/15877[#15877]

=== 3D Viewer
- Fix crash with certain 3D models. https://gitlab.com/kicad/code/kicad/-/issues/15336[#15336]

=== Calculator Tools
- Correct transmission line conductor loss calculation. https://gitlab.com/kicad/code/kicad/-/issues/15873[#15873]

=== Command Line Interface
- Fix doubled text variable substitution. https://gitlab.com/kicad/code/kicad/-/issues/15765[#15765]

=== Windows
- https://gitlab.com/kicad/code/kicad/-/commit/bf2e34f32ee8dfa195ec20d06af161e50710146c[Change variable substitution for path separators].
- https://gitlab.com/kicad/code/kicad/-/commit/a18910262850400cc69107a133f4567a4a21d68e[Bump wxWidgets and Curl versions].

=== macOS
- Fix external text editor command failure. https://gitlab.com/kicad/code/kicad/-/issues/15326[#15326]
- Fix stand alone mode crash when quitting using the dock bar menu. https://gitlab.com/kicad/code/kicad/-/issues/15024[#15024]
