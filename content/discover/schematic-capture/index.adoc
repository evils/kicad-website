+++
title = "Schematic Capture in Eeschema"
tags = ["schematic capture",
        "eagle schematic"
        ]
aliases = [
    "/discover/eeschema/"
]
[menu.main]
    parent = "Discover"
    name   = "Schematic Capture"
	weight = 1
+++


KiCad's schematic capture includes all the tools you need for schematic design.
Place symbols from KiCad's included symbol library, draw wires to make circuits and
manipulate your schematic drawing to make a final engineering design to take into PCB
layout.

<!--more-->

++++
<div class="text-center ratio ratio-16x9">
    <video autoplay loop muted playsinline class="embed-responsive-item">
        <source src="basic.webm" type="video/webm">
        <source src="basic.mp4" type="video/mp4">
    </video>
</div>
++++



== Electrical Rules Check

The electrical rules check (ERC) automatically verifies your schematic connections.
It checks for output pin conflicts, missing drivers and unconnected pins.


++++
<div class="text-center ratio ratio-16x9">
    <video autoplay loop muted playsinline class="embed-responsive-item">
        <source src="erc.webm" type="video/webm">
        <source src="erc.mp4" type="video/mp4">
    </video>
</div>
++++

== Export Netlists

Exports netlists in formats such as

- Pspice
- Cadstar
- PcbNew
- "Generic" XML

++++
<div class="text-center ratio ratio-16x9">
    <video autoplay loop muted playsinline class="embed-responsive-item">
        <source src="netlist.webm" type="video/webm">
        <source src="netlist.mp4" type="video/mp4">
    </video>
</div>
++++

== Integrated libraries

KiCad comes bundled with a vast library of symbols, footprints, and
matching 3D models. They are community maintained so they never stop
improving.

++++
<div class="text-center ratio ratio-16x9">
    <video autoplay loop muted playsinline class="embed-responsive-item">
        <source src="libraries.webm" type="video/webm">
        <source src="libraries.mp4" type="video/mp4">
    </video>
</div>
++++


== Import / migrate from other CAD tools

KiCad currently supports schematic designs from:

* EAGLE
* Altium Circuit Maker
* Altium Circuit Studio
* Altium Designer
* CADSTAR
