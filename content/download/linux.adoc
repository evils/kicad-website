+++
title = "Install on Linux"
distro = "Linux/Other"
summary = "Install instructions for KiCad on Linux"
iconhtml = "<div class='fl-tux'></div>"
weight = 3
+++

== About the Release Channels

**Stable releases** are the official tagged KiCad versions.  Every year, a new major release (the
first version number) comes out, bringing new features and bug fixes, but also usually bringing
file format changes that prevent downgrading.  In other words, once you update to 7.x and start
modifying a project, you will no longer be able to open that project in any 6.x version.

The last number in the version is the patch (bugfix) release version.  We encourage users to update
to the latest bugfix releases regularly.

**Testing builds** are nightly builds from the stable release branch.  They contain changes that
will become the next bugfix release (for example, when the current stable release is 7.0.0, the
testing releases contain fixes that will eventually become 7.0.1).  Testing releases install on top
of (replace) the stable releases.

**Nightly builds** are built from the development branch that will become the next stable KiCad
release.  They contain new features as well as bug fixes, but may also contain more bugs than the
other channels.  Nightly builds can be installed alongside stable/testing builds on most platforms.

WARNING: Please read link:/help/nightlies-and-rcs/[Nightly Builds and Release Candidates] for
	   important information about the risks and drawbacks of using nightly builds.

== Officially-supported Platforms

=== Ubuntu

KiCad operates link:https://help.ubuntu.com/community/PPA[PPAs] that can be used to install KiCad
along with its required dependencies.  We recommend that users install from these PPAs rather than
the base Ubuntu repository as the latter is usually outdated.

NOTE: There are many distributions based on Ubuntu that can make use of these PPAs, but only the
      official Ubuntu flavors from Canonical are supported by the KiCad team.  Linux Mint users:
      see the <<Linux Mint>> section below.

*{{< param "release" >}} Stable Release*

[source,bash]
----
sudo add-apt-repository ppa:kicad/kicad-7.0-releases
sudo apt update
sudo apt install kicad
----

*Testing Builds*

[source,bash]
----
sudo add-apt-repository ppa:kicad/kicad-7.0-nightly
sudo apt update
sudo apt install kicad
----

*Nightly Builds*

[source,bash]
----
sudo add-apt-repository ppa:kicad/kicad-dev-nightly
sudo apt update
sudo apt install kicad-nightly
----

=== Fedora

*Stable Release*
[cols="1,2"]
|===
|Rawhide   | &nbsp;{{< repology fedora_rawhide >}}
|Fedora 39 | &nbsp;{{< repology fedora_39 >}}
|Fedora 38 | &nbsp;{{< repology fedora_38 >}}
|===

To install the version available in your Fedora release:

[source,bash]
dnf install kicad kicad-packages3d kicad-doc

Additionally, there are three Copr repositories for KiCad, providing alternate versions as discussed below.

Enabling multiple copr repositories is fine, but please note that if you choose to enable both the testing copr and
the stable copr, they will effectively be combined, meaning that an update will come from the one that has the newer build.
Usually this will be the testing copr.

The nightly copr is independent of the other two.

*Stable Builds*

To install the latest stable build from link:https://copr.fedorainfracloud.org/coprs/g/kicad/kicad-stable/[Copr]:

[source,bash]
dnf install dnf-plugins-core
dnf copr enable @kicad/kicad-stable
dnf install kicad kicad-packages3d kicad-doc

*Testing Builds*

To install the latest testing build of the stable branch from link:https://copr.fedorainfracloud.org/coprs/g/kicad/kicad-testing/[Copr]:

[source,bash]
dnf install dnf-plugins-core
dnf copr enable @kicad/kicad-testing
dnf install kicad kicad-packages3d kicad-doc

*Nightly Builds*

To install the latest nightly build of the development branch from link:https://copr.fedorainfracloud.org/coprs/g/kicad/kicad/[Copr]:

[source,bash]
dnf install dnf-plugins-core
dnf copr enable @kicad/kicad
dnf install kicad-nightly kicad-nightly-packages3d

=== FlatPak

FlatPak is a software distribution system that allows us to provide up-to-date versions of KiCad on
many Linux distributions.  FlatPak is officially supported by KiCad and is our recommended way to
install the latest version of KiCad on any distribution other than Ubuntu and Fedora, if it is not
available through your distribution's package manager.

*Stable Release* image:https://img.shields.io/flathub/v/org.kicad.KiCad?color=bright[]

image:https://flathub.org/assets/badges/flathub-badge-en.png[width=240px, link='https://flathub.org/repo/appstream/org.kicad.KiCad.flatpakref']

Or install from the terminal:

[source,bash]
flatpak install --from https://flathub.org/repo/appstream/org.kicad.KiCad.flatpakref

== Testing Builds
Testing builds (i.e. 7.0.x-rcy) are hosted on the flathub-beta branch.

image:https://flathub.org/assets/badges/flathub-badge-en.png[width=240px, link='https://flathub.org/beta-repo/appstream/org.kicad.KiCad.flatpakref']

Or install from the terminal:

[source,bash]
flatpak install --from https://flathub.org/beta-repo/appstream/org.kicad.KiCad.flatpakref

== Switching between Flathub's stable and beta branches

If KiCad is installed via both branchen, run the desired version explicitly by:

[source,bash]
flatpak run org.kicad.KiCad//stable
flatpak run org.kicad.KiCad//beta

If you want to switch which version your desktop environment shows in menus, use
link:https://docs.flatpak.org/en/latest/flatpak-command-reference.html#flatpak-make-current[`flatpak make-current`].

=== Debian

*Stable Release*
[cols="1, 1"]
|===
|Bookworm (via link:https://backports.debian.org/Instructions/[Backports])  | &nbsp;{{< repology debian_12 >}}
|Sid        | &nbsp;{{< repology debian_unstable >}}
|===

[source,bash]
----
sudo apt install kicad
----

== Other Platforms

The following platforms are not officially supported by the KiCad team, but may be supported by the
community of users on that platform.  Bugs encountered on these platforms must be reproduced on a
supported platform before the KiCad core team will investigate them.  See the
link:/help/system-requirements/[System Requirements] page for more details.

=== Arch Linux

*Stable Release*

{{< repology arch >}}

[source,bash]
----
sudo pacman -Syu kicad
sudo pacman -Syu --asdeps kicad-library kicad-library-3d
----

*Nightly Builds*

Available from AUR at https://aur.archlinux.org/packages/kicad-nightly/[`aur/kicad-nightly`]

=== FreeBSD

*Stable Release*

{{< repology freebsd >}}

[source,bash]
pkg install kicad

=== Gentoo

*Stable Release*

{{< repology gentoo >}}

[source,bash]
emerge sci-electronics/kicad

=== openSUSE

*Stable Release*

{{< repology opensuse_tumbleweed >}}

[source,bash]
zypper addrepo https://download.opensuse.org/repositories/electronics/openSUSE_Tumbleweed/electronics.repo
zypper refresh
zypper install kicad

=== Linux Mint

Linux Mint users can use the Ubuntu PPAs to install KiCad.  It is important to note that Linux Mint
is different enough from Ubuntu that it is not a supported platform, and may have bugs that do not
appear in Ubuntu.

*{{< param "release" >}} Stable Release*

[source,bash]
----
sudo add-apt-repository ppa:kicad/kicad-7.0-releases
sudo apt update
sudo apt install kicad
----

*Testing Builds*

[source,bash]
----
sudo add-apt-repository ppa:kicad/kicad-7.0-nightly
sudo apt update
sudo apt install kicad
----

*Nightly Builds*

[source,bash]
----
sudo add-apt-repository ppa:kicad/kicad-dev-nightly
sudo apt update
sudo apt install kicad-nightly
----
