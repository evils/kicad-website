+++
title = "Nightly Builds and Release Candidates"
summary = "Information for users of nightly builds and the KiCad release candidate process"
aliases = [ "/help/nightlies-and-rcs/" ]
[menu.main]
    parent = "Help"
    name   = "Nightly Builds and Release Candidates"
  weight = 50
+++
:toc: macro
:toc-title:

toc::[]

== Nightly Builds

KiCad provides nightly builds of the development branch for all supported platforms.  These builds
are provided so that users may test newly-introduced features and provide feedback.

WARNING: These builds may contain serious bugs including those that can cause loss of data.  Make
         sure to have a backup and/or version control system in place before using nightly builds
         on any design.

Since nightly builds are intended to be used for testing new and potentially unstable features,
users running these builds are highly encouraged to create an account in GitLab so that bugs may be
reported.

KiCad nightly versions always have a minor version of 99.  So, nightly versions of the development
branch after version 6.0 is released will always be called 6.99.  The full version string shown in
the About dialog contains information about the specific nightly build.

NOTE: Users who install KiCad nightly builds using a package manager must take care when KiCad
      releases a new stable version.  As soon as the stable version is released, the nightly builds
      will become open to changes for the next stable version, and it is typical for these changes
      to come rapidly at the start of a development cycle.  This means that users who have been
      relying on the relative stability of nightly builds late in the release cycle and during the
      Release Candidate process may want to switch from the nightly to the stable release channels
      of their package manager once the stable release is published.

=== Compatibility and file formats

KiCad has a policy of ensuring backwards-compatibility and does not provide forwards-compatiblity
for file formats.  What this means is that a newer version of KiCad should always be able to open
any files created in an older version of KiCad.  The opposite is not true, in general: files
created using a newer version of KiCad will not be readable by older versions.

WARNING: Opening projects or libraries in a nightly build and then saving them will result in files
         that can no longer be used in the stable release of KiCad.  Always operate on copies of
         files when testing nightlies alongside the stable release.

== Release Candidates

As the KiCad team prepares for a new major version release, KiCad will go through a release
candidate stage to signal to users that we are close to a release and to encourage more extensive
user testing and bug reporting.

Unlike some software projects, KiCad does not make special release candidate builds that are
"frozen" for some period of time to collect feedback.  Instead, KiCad release candidates are just
nightly builds with the version number changed.  So, there is no "release candidate version" to
install for users wishing to test KiCad during this stage: those users should simply install the
latest nightly build.

Nightly builds will continue to be published after each release candidate, fixing bugs that have
been found in prior builds.  Users testing KiCad should continue to update regularly, as there may
be bugs found in the tagged release candidate build that are fixed in later nightly builds.

Starting from the first release candidate tag, KiCad will start to identify itself as the next
major version in places where only the major and minor version are shown.  For example, KiCad
nightly builds after version 6 is released will normally be called version 6.99, but as soon as the
first release candidate tag is created, the nightly builds will show 7.0 in the titlebar.

As always, the full version can be accessed in the About dialog.  A nightly build based on a
release candidate tag will have a version like `7.0.0-rc1-378-ge76fd128c3`.  The first portion of
this version, `7.0.0-rc1`, indicates that the most recent KiCad tag was `7.0.0-rc1`, so this build
is from sometime *after* that RC1 release.  The second portion, `378-ge76fd128c3`, identifies the
exact nightly build version.

Because the major version number is changed to 7.0 from 6.99 in this example, the first time a user
installs a KiCad nightly build after the RC1 tag, KiCad will start using a new settings location
(since KiCad settings are separated by major and minor version) and offer to import settings from
the previous version (6.99 in this example).  Any changes made to the symbol and footprint library
configurations, as well as any custom content installed in the previous nightly will need to be
repeated.
