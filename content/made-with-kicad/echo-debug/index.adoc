+++
title = "Echo Debug"
projectdeveloper = "Petr Hodina"
projecturl = "https://gitlab.com/phodina/echo-debug-gen3"
"made-with-kicad/categories" = [
    "USB-Device"
]
+++

Echo Debug allows you to connect to the Amazon Echo Gen 3 device and access the fastboot protocol. 
