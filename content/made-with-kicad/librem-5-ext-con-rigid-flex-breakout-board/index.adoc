+++
title = "Librem 5 EXT CON Rigid-Flex Breakout Board"
projectdeveloper = "Purism"
projecturl = "https://source.puri.sm/Librem5/ext-con-breakout-board"
"made-with-kicad/categories" = [
    "Rigid Flex"
]
+++

The link:https://shop.puri.sm/shop/librem-5-rigid-flex-breakout-board/[EXT CON Rigid-Flex Breakout Board] is a small board that connects to the FPC connector J10 on the Librem 5 while the rigid section mounts inside of the phone using the center M.2 cover-plate threaded insert.
The breakout board provides UART, I2C, SPI, two GPIOs, 3.3V, and 1.8V; making it really useful for custom projects, as demonstrated in link:https://puri.sm/posts/expand-the-librem-5-hardware-with-the-breakout-board/[this blog post].
